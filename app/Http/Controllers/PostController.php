<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Traits\UploadTrait;

class PostController extends Controller
{
    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $posts = Post::all();
       return view('post.index',compact('posts',$posts));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            
        $validatedData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'date' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif|max:2048',
            // 'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);


        $image = $request->file('image');
        $name = $image->getFilename().'_'.time();
        $folder = 'uploads/images';
        $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
        $this->uploadOne($image, $folder, 'public', $name);
        $extension = $image->getClientOriginalExtension();
        $post = new Post();
        $post->title = $request->title;
        $post->description = $request->description;
        $post->date = $request->date;
        $post->image = $name;
        $post->save();

        return redirect()->route('post.create')
        ->with('success','Post created successfully...');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($post_id)
    {
        $posts = Post::find($post_id);
        return view('post.show',compact('posts',$posts));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
