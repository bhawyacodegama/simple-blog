<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment;
        $comment->body = $request->comment_body;
        $post = Post::find($request->post_id);
        $post->comments()->save($comment);

        return back();
    }
}
