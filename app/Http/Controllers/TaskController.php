<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index() {
    	$task = Task::findall();
    	return view('task.show',array('task' => $task));
    }
}
