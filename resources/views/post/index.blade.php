@extends('layouts.app')

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <h3>Uploaded Posts</h3>
        @foreach($posts as $key=>$post)
            @if($key%2 == 0)
            <div>
                <div style="float:left;">
                    <h3>{{$post->title}}</h3>
                    <p>
                        {{ substr(strip_tags($post->description), 0, 700) }}
                        @if (strlen(strip_tags($post->description)) > 50)
                        ...
                        <a href="{{ route('post.show', $post->id) }}" class="btn btn-primary">Show Post</a>
                            @endif
                    </p>
                </div>
                <div style="float:right;" >
                    <img src="{{url('uploads/images/'.$post->image)}}" alt="{{$post->image}}" style="width:200px;height:100px;"></img>
                </div>
            </div>
            @else
            <div>
                <div style="float:right;">
                    <h3>{{$post->title}}</h3>
                    <p>
                        {{ substr(strip_tags($post->description), 0, 600) }}
                        @if (strlen(strip_tags($post->description)) > 60)
                        ... <a href="{{ route('post.show', $post->id) }}" class="btn btn-primary">Show Post</a>
                        @endif
                    </p>
                </div>
                <div style="float:left;">
                    <img src="{{url('uploads/images/'.$post->image)}}" alt="{{$post->image}}" style="width:200px;height:100px;"></img>
                </div>
            </div>
            @endif
        @endforeach
      
@endsection