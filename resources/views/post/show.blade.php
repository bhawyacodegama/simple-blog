@extends('layouts.app')

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <h3>Posts View</h3>
    <div>
        <div>
            <h3>{{$posts->title}}</h3>
            <p>{{$posts->description}}
            </p>
        </div>
        <div>
            <img src="{{url('uploads/images/'.$posts->image)}}" alt="{{$posts->image}}" style="width:500px;height:300px;"></img>
        </div>
		<br>
        <h4>Display Comments</h4>
            @foreach($posts->comments as $comment)
                <div class="display-comment">
                    <p>{{ $comment->body }}</p>
                </div>
            @endforeach

        <h4>Add comment</h4>
        <form method="post" action="{{ route('comment.store') }}">
            @csrf
            <div class="form-group">
                <input type="text" name="comment_body" class="form-control" />
                <input type="hidden" name="post_id" value="{{ $posts->id }}" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-warning" value="Add Comment" />
            </div>
        </form>
    </div>
@endsection