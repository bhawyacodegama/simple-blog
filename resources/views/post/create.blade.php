@extends('layouts.app')

@section('content')
<div class="container">
	<h1>Create Post</h1>
    <h3>Add New Post</h3>
    <hr>
    <form action="{{url('/post')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title"  name="title">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea type="text" class="form-control" id="description" name="description"></textarea>
        </div>
        <div class="form-group">
            <label for="date">Date</label>
            <input type="date" class="form-control" id="date" name="date"/>
        </div>
        <div class="form-group">
            <label for="image">Image1</label><br/>
            <input id="image" type="file" class="form-control" name="image">
        </div>
        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
