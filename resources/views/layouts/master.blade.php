<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
     <link rel="stylesheet" href="/css/bootstrap.min.css">
    @yield('styles')
</head>
<body>
@yield('content')

<script type="text/javascript" src="{{ URL::to('js/bootstrap.min.js') }}"></script>
@yield('scripts')
</body>
</html>